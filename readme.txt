사용법
1. install virtualenv
설치할 디렉토리로 이동(예: ~/tensorflow)
$sudo apt-get install python-pip python-dev python-virtualenv

활성화방법
$source (설치경로)/bin/activate #일반적인 경우
$source ./tensorflow/bin/activate #현준이의 경우

비활성화 방법
$deactivate

1. Install Tensorflow
먼저 Virtual-Env환경을 activate 시켜준다.

$source ./tensorflow/bin/activate
이렇게하면 프롬프트 창의 이름앞에 (tensorflow)가 들어가있다.
$(tensorflow)root@hyunjun-virtual-machine:


virtualenve 환경에서 Tensorflow를 설치한다.(CPU버전)

#For CPU-only version
$pip install https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-0.5.0-cp27-none-linux_x86_64.whl


# For GPU-enabled version (only install this version if you have the CUDA sdk installed)
$ pip install https://storage.googleapis.com/tensorflow/linux/gpu/tensorflow-0.5.0-cp27-none-linux_x86_64.whl

3. Scipy를 설치

sudo apt-get install python-numpy python-scipy python-matplotlib ipython ipython-notebook python-pandas python-sympy python-nose

4. 바꿔줄 이미지파일을 examples 폴더로 옮겨준다.
cp /mnt/e/hyunjun.jpg ~/neural-style/examples

5. 실행명령어
python neural_styple.py --content examples/hyunjun.jpg --styles examples/2-styple2.jg --output result.jpg
